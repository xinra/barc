﻿xinra barc - Barely a Real CMS
==============================

barc is a simple and lightweight PHP content management system.  
I do regularly develop small websites that share the same basic PHP setup. I created the open source
barc project as a reusable scaffolding that provides some basic functionality required by almost every
website. If you actually decided to use barc for one of your projects, it would be kind to let me know that
this readme wasn't written for nothing.  

Features
--------
* __Template "Engine"__
	* Define a template that is used on all pages
	* Already useful for the most basic type of projects - a static website with more than one page
* __Pretty URLs__
	* The URL structure is `http://<domain>/<language>/<page>/<arg_1>/.../<arg_n>`
	* For example: `http://example.com/en/user/42`
	* You can also use classic GET parameters instead of pretty args
* __Internationalization (i18n)__
	* Easily translate your website into other languages
* __Configuration__
	* Settings are bundled in a configuration file
* __User System__
	* Basic implementation of user management
	* Information saved in MySQL database
	* Features
		* Register
		* Verify E-Mail Address
		* Log in
		* Reset Password
		* Log out
		* Edit account information
* __Customizable 404 Error Page__
* __Protection__
	* Only your specified pages and the `assets/` directory are accessable
	* Prepared statements are used for database access

Requirements
------------
* PHP 5 (>= 5.5.0 if you use the user system with default hash methods)
* MySQL Database if you use the user system

Getting Started
---------------
* [Download](https://bitbucket.org/xinra/barc/downloads) the latest version and unzip it into your development environment.
* Go through `inc/config.php` and customize the settings (If you don't want to use the user system, remove the according entries from `$barc_available_pages`. You can optionally delete the files in `pages/`).
* If you want to use the user system, execute `install.php` (You have to rename `.htaccess` temporarily to access it).
* Set up your template in `inc/template.php` (images, css & js files etc. go into `assets/`).
* Add pages to `pages/` and to `$barc_available_pages` (custom utility functions go into `inc/functions.php` which is automatically loaded, contents due to translation go into `locale/<language>/page.<page name>.php` which are automatically loaded).
* I would advise you to read through the sample content, especially the user system, to learn how things work.

Exposed Constants, Functions and Variables
------------------------------- 
* Contants
	* `BASE_URL` as defined in the config, use it to reference your `assets`.
	* `DB_PRE` as defined in the config, use it for all table names in SQL queries.
	* `URL` base url with applied language parameter, use it to reference your `pages`.  

* Functions
	* `barc_load_translation($name)` loads a custom translation from `locale/<language>/<name>.php`.
	* `barc_logged_in()` returns wether the user is logged in or not (i.e. if $_SESSION["user_id"] is set).
	* `barc_open_db()` returns a `mysqli` object that is connected to the database. The first time you call this, the connection is established and reused from there on. The connection will be closed automatically.
	* `barc_require_login()` call this at the start of each page that requires the user to be logged in. If there's already a pending verification for the specified user, it will be overwritten. Returns the key that is used to verify the e-mail (YOU STILL HAVE TO SEND IT VIA E-MAIL YOURSELF). The expiration date for this activation can be set in the config.
	* `barc_verify_email()` generates an entry in the `email_verifications` table so that a user can verify the e-mail address upon registration or e-mail address change. 
	* `session_destroy()` will log out the user.
	* `t($key)` i18n function, returns translation for the specified key.  

* Golbal Variables (Remember to use `global $var;` inside of functions!)
	* `$barc_args` array of arguments as resolved by the pretty URL feature (Note that a trailing `/` causes an empty arg).
	* `$barc_lang` the language that it currently used.
	* `$barc_selectlang` boolean, true if it's the language selection page and hence no i18n is available.
	* `$_SESSION["user_id"]` contains the ID of the logged in user or is not set, if no user is logged in.

License
-------
barc is published under the MIT License. See LICENSE.txt for details.