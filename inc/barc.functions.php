<?php

//Call this at the start of each page that requires the user to be logged in
function barc_require_login() {
    global $barc_args;
    global $barc_page;
    if(!barc_logged_in()) {
        header("HTTP/1.0 401 Unauthorized");
        header("Location: ".URL."/login/" . $barc_page . "/" . join("/", $barc_args));
        die;
    }
}

//Returns wether the user is logged in or not
function barc_logged_in() {
    return isset($_SESSION['user_id']);
}

//Closes current database connection if there is one
//It is automatically called at the end of script execution
function barc_close_db() {
    global $barc_db;
    if(isset($barc_db)) {
        $barc_db->close();
    }
}

//Open a database connection if there isn't one already
//The connection is stored in the global variable $barc_db (it is also returned by the function)
function barc_open_db() {
    global $barc_db;
    global $barc_db_settings;
    if(!isset($barc_db)) {
        $barc_db = new mysqli($barc_db_settings["host"], $barc_db_settings["user"], $barc_db_settings["password"], $barc_db_settings["database"]);
        if ($barc_db->connect_errno) {
            printf("Database Connection Error: %s\n", $barc_db->connect_error);
            die;
        }
        register_shutdown_function("barc_close_db");
    }   
    return $barc_db; 
}

//i18n method - Returns the translation with the given key
function t($id) {
    global $barc_translation;
    if(isset($barc_translation[$id])) {
        return $barc_translation[$id];
    } else {
        return "[" . $id . "]";
    }
}

//Load a custom translation from locale/<language>/<name>.php
function barc_load_translation($name) {
    global $barc_lang;
    global $barc_translation;
    if(file_exists("locale/".$barc_lang."/".$name.".php")) {
        include_once("locale/".$barc_lang."/".$name.".php");    
    }    
}

//Generates an entry in the email_verifications table so that a user can verify the e-mail address upon registration or e-mail address change
//If there's already a pending verification for the specified user, it will be overwritten
//Returns the key that is used to verify the e-mail (YOU STILL HAVE TO SEND IT VIA E-MAIL YOURSELF)
//The expiration date for this activation can be set in the config
function barc_verify_email($user_id, $email) {
    global $barc_activation_expires;
    $db = barc_open_db();
    $query = $db->prepare("REPLACE INTO `" . DB_PRE . "email_verifications` (`user_id`, `email`, `secret`, `expires`) VALUES (?, ?, ?, ?);");
    $expires = date("Y-m-d", time()+$barc_activation_expires);
    $key = md5(password_hash(time(), PASSWORD_DEFAULT)); //this way the salt will be randomly generated
    $query->bind_param('isss', $user_id, $email, $key, $expires);
    $query->execute();
    $query->close();
    return $key;    
}
                                          
?>