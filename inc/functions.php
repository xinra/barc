<?php

//In this file you can define your own utility function


//Sample function that creates the form for register and account pages
//true = register, false = account settings
function user_form($register) {
    $form = '<form action="' . URL . '/' . ($register ? 'register' : 'account') . '" method="post">';
        $form .= '<p><label for="email">' . t('user.email') . '</label> <input type="email" id="email" name="email"' . (isset($_POST['email']) ? ' value="' . htmlspecialchars($_POST['email']) . '"' : '') . ' /></p>';
        $form .= '<p><label for="repeat-email">' . t('user.repeat-email') . '</label> <input type="email" id="repeat-email" name="repeat-email"' . (isset($_POST['repeat-email']) ? ' value="' . htmlspecialchars($_POST['repeat-email']) . '"' : '') . ' /></p>';
        $form .= '<p><label for="password">' . t('user.password') . '</label> <input type="password" id="password" name="password" /></p>';
        $form .= '<p><label for="repeat-password">' . t('user.repeat-password') . '</label> <input type="password" id="repeat-password" name="repeat-password" /></p>';
        //For register you may want to include a captcha here
        $form .= '<p><input type="submit" value="' . t(($register ? 'register' : 'account') . '.button') . '" name="submit" /></p>';
    $form .= '</form>';
    return $form;
}


?>