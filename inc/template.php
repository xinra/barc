<?php 

function print_template($content, $head = false) {
global $barc_selectlang;

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>barc Sample Template</title>
    
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/style.css" />

    <?php if($head != false) echo $head; ?>

  </head>
  <body>
    <h1>barc Sample Template</h1>
    <?php if(!$barc_selectlang) : ?>
        <div class="container">
            <b style="margin-right: 20px;">Navigation:</b> 
            <a href="<?php echo URL; ?>"><?php echo t("nav.home"); ?></a> &bull;
            <?php if(!barc_logged_in()) : ?>
                <a href="<?php echo URL; ?>/login"><?php echo t("nav.login"); ?></a> &bull;
                <a href="<?php echo URL; ?>/register"><?php echo t("nav.register"); ?></a> &bull;
            <?php else : ?>
                <a href="<?php echo URL; ?>/account"><?php echo t("nav.account"); ?></a> &bull;
                <a href="<?php echo URL; ?>/logout"><?php echo t("nav.logout"); ?></a> &bull;
            <?php endif; ?>
            <a href="<?php echo BASE_URL; ?>"><?php echo t("nav.selectlang"); ?></a>
        </div>
    <?php endif; ?>
    <div class="container">
        <?php echo $content; ?>
    </div>  
  </body>
</html>

<?php     
}

 ?>