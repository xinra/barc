<?php

$barc_translation["nav.home"]            = "Home";
$barc_translation["nav.login"]           = "Login";
$barc_translation["nav.logout"]          = "Logout";
$barc_translation["nav.register"]        = "Register";
$barc_translation["nav.account"]         = "Account";
$barc_translation["nav.selectlang"]      = "Change Language";

$barc_translation["format.date"]         = "d/m/Y";

?>