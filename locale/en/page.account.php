<?php

//This is a sample page translation. For each site a translation page.<page name> is loaded automatically.

$barc_translation["account.button"]             = 'Save Settings';
$barc_translation["account.success.email"]      = 'Your e-mail has been updated. A verification link has been sent to the new address. Until you verified the new address you need to use the old one to log in!';
$barc_translation["account.success.password"]   = 'Your password has been updated.';
$barc_translation["account.email"]              = 'Current E-Mail: %s';
$barc_translation["account.email.content"]      = 'You have changed your barc e-mail address to this one. Please verify this at <a href="%1$s" title="Verify E-Mail">%1$s</a>.';
$barc_translation["account.email.subject"]      = 'barc E-Mail Address Change';

?>