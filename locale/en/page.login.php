<?php

$barc_translation["login.button"]            = "Login";
$barc_translation["login.error"]             = "Username or password is not valid!";
$barc_translation["login.logged-in"]         = "You are already logged in!";
$barc_translation["login.required"]          = "You need to be logged in to access this page!";
$barc_translation["login.success"]           = "You've successfully logged in. You will be redirected in a few seconds.";
$barc_translation["login.reset-password"]    = "Forgot your password?";

?>