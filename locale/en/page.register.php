<?php

//This is a sample page translation. For each site a translation page.<page name> is loaded automatically.

$barc_translation["register.button"]             = 'Register';
$barc_translation["register.success"]            = 'Registration successful! An activation link has been sent to your e-mail address.';
$barc_translation["register.email.content"]      = 'Thank you for registering. Please activate your account at %s.';
$barc_translation["register.email.subject"]      = 'barc Registration';

?>