<?php

$barc_translation["reset-password.title"]             = "Reset Password";

$barc_translation["reset-password.reset.button"]      = "Reset Password";
$barc_translation["reset-password.reset.error"]       = "There is no account with the specified e-mail address!";
$barc_translation["reset-password.reset.success"]     = "A link to reset your password has been sent to your e-mail address.";
$barc_translation["reset-password.email.content"]     = 'You requested a password reset. To set a new password for your barc account visit %s.';
$barc_translation["reset-password.email.subject"]     = 'barc Password Reset';

$barc_translation["reset-password.save.button"]       = "Update Password";
$barc_translation["reset-password.save.error" ]       = "The reset key is invalid or expired.";
$barc_translation["reset-password.save.success"]      = "Your password has been updated.";


?>