<?php

//This is a sample custom translation. Custom translations can be loaded using barc_load_translation(<name without .php>)

$barc_translation["user.name"]                    = "Username:";
$barc_translation["user.email"]                   = "E-Mail:";
$barc_translation["user.repeat-email"]            = "Repeat E-Mail:";
$barc_translation["user.password"]                = "Password:";
$barc_translation["user.repeat-password"]         = "Repeat Password:";
$barc_translation["user.member-since"]            = "Member since %s";
$barc_translation["user.save"]                    = "Save Account Settings";
$barc_translation["user.register"]                = "Register";
$barc_translation["user.login"]                   = "Login";

$barc_translation["user.error.email"]             = "E-Mail is not valid!";
$barc_translation["user.error.repeat-email"]      = "E-Mails are not the same!";
$barc_translation["user.error.email-exists"]      = "An account with this e-mail does already exist!";
$barc_translation["user.error.password"]          = "Password is not valid!";
$barc_translation["user.error.repeat-password"]   = "Passwords are not the same!";

?>