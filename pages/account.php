<?php

barc_require_login();
barc_load_translation("user");

$db = barc_open_db();
$query = $db->prepare("SELECT `email`, `member_since` FROM `".DB_PRE."users` WHERE `id` = ? LIMIT 1;");
$query->bind_param("i", $_SESSION['user_id']);
$query->execute();
$query->bind_result($email, $member_since);
$query->fetch();
$query->close();

$error = "";
$success = "";
if(isset($_POST['submit'])) {
    if(isset($_POST['email']) && !empty($_POST['email'])) {
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false || strlen($_POST['email']) > 255) {
            $error .= '<li>' . t("user.error.email") . '</li>';
        } elseif(!isset($_POST['repeat-email']) || $_POST['email'] != $_POST['repeat-email']) {
            $error .= '<li>' . t("user.error.repeat-email") . '</li>';
        } else {
            $query = $db->prepare("SELECT `id` FROM `".DB_PRE."users` LEFT JOIN `".DB_PRE."email_verifications` ON `user_id` = `id` WHERE UPPER(`".DB_PRE."users`.`email`) = UPPER(?) OR UPPER(`".DB_PRE."email_verifications`.`email`) = UPPER(?) LIMIT 1;");
            $query->bind_param("ss", $_POST['email'], $_POST['email']);
            $query->execute();
            $query->bind_result($id);
            $query->fetch();
            $query->close();
            if($id != 0) {
                $error .= '<li>' . t("user.error.email-exists") . '</li>';
            } else {
                $key = barc_verify_email($_SESSION['user_id'], $_POST['email']); 
                mail($_POST['email'], t("account.email.subject"), sprintf(t("account.email.content"), URL . "/verify-email/" . $key), "From: " . $barc_email_from);
                $success .= '<li>' . t("account.success.email") . '</li>';   
            }
        }
    }

    if(isset($_POST['password']) && !empty($_POST['password'])) {
        if(!isset($_POST['repeat-password']) || $_POST['password'] != $_POST['repeat-password']) {
            $error .= '<li>' . t("user.error.repeat-password") . '</li>';
        } else {
            $query = $db->prepare("UPDATE `".DB_PRE."users` SET `password` = ? WHERE `id` = ?;");
            $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $query->bind_param('si', $hash, $_SESSION['user_id']);
            $query->execute();
            $query->close();
            $success .= '<li>' . t("account.success.password") . '</li>';    
        }
    }
}

$content = "<h2>".t("nav.account")."</h2>";
$content .= '<p>'.sprintf(t("user.member-since"), date(t("format.date"), strtotime($member_since))).'<br />';
$content .= sprintf(t("account.email"), htmlspecialchars($email)) . '</p>';


if(!empty($error)) {
    $content .= '<ul class="error">' . $error . '</ul>';
} 
if(!empty($success)) {
    $content .= '<ul class="success">' . $success . '</ul>';
}

$content .= user_form(false);

if($barc_dev_mode && isset($key)) {
    $content .= '<p><a href="'. URL . "/verify-email/" . $key . '">[DEV VERIFY]</a></p>';
}

print_template($content);