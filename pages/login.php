<?php

if(barc_logged_in()) {
    print_template('<p class="error">' . t("login.logged-in") . '</p>');
    die;
}

barc_load_translation("user");

if(isset($_POST['submit'])) {
    if(!isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['password']) || empty($_POST['password'])) {
        $error = t("login.error");
    } else {
        $db = barc_open_db();
        $query = $db->prepare("SELECT `id`, `password` FROM `".DB_PRE."users` WHERE UPPER(`email`) = UPPER(?) LIMIT 1;");
        $query->bind_param("s", $_POST['email']);
        $query->execute();
        $query->bind_result($id, $hash);
        $query->fetch();
        $query->close();
        if($id == 0 || !password_verify($_POST['password'], $hash)) {
            $error = t("login.error");
        } else {
            $_SESSION['user_id'] = $id;
        }
    }
} elseif(isset($barc_args[0]) && !empty($barc_args[0])) {
    $error = t("login.required");
}

$content = '<h2>'.t("nav.login").'</h2>';

if(!isset($_POST['submit']) || isset($error)) {
    if(isset($error)) {
        $content .= '<p class="error">' . $error . '</p>';
    }
    $content .= '<form action="' . URL . '/login/'.join("/", $barc_args).'" method="post">';
        $content .= '<p><label for="email">' . t('user.email') . '</label> <input type="email" id="email" name="email"' . (isset($_POST['email']) ? ' value="' . htmlspecialchars($_POST['email']) . '"' : '') . ' /></p>';
        $content .= '<p><label for="password">' . t('user.password') . '</label> <input type="password" id="password" name="password" /></p>';
        $content .= '<p><input type="submit" value="' . t('login.button') . '" name="submit" /> <a href="'.URL.'/reset-password" title="'.t("login.reset-password").'">'.t("login.reset-password").'</a></p>';
    $content .= '</form>';
    $head = false;                                                                                  
} else {
    $content .= '<p class="success">' . t("login.success") . '</p>';
    $head = '<meta http-equiv="refresh" content="5; URL='.URL.'/'.join("/", $barc_args).'">';
}

print_template($content, $head);

?>