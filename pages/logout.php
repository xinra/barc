<?php

session_unset();
session_destroy();

print_template('<p class="success">'.t("logout.success").'</p>');

?>