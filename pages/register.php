<?php

barc_load_translation("user");

if(isset($_POST['submit'])) {
    $error = "";
    if(!isset($_POST['email']) || filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false || strlen($_POST['email']) > 255) {
        $error .= '<li>' . t("user.error.email") . '</li>';
    } elseif(!isset($_POST['repeat-email']) || $_POST['email'] != $_POST['repeat-email']) {
        $error .= '<li>' . t("user.error.repeat-email") . '</li>';
    } else {
        $db = barc_open_db();
        $query = $db->prepare("SELECT `id` FROM `".DB_PRE."users` LEFT JOIN `".DB_PRE."email_verifications` ON `user_id` = `id` WHERE UPPER(`".DB_PRE."users`.`email`) = UPPER(?) OR UPPER(`".DB_PRE."email_verifications`.`email`) = UPPER(?) LIMIT 1;");
        $query->bind_param("ss", $_POST['email'], $_POST['email']);
        $query->execute();
        $query->bind_result($id);
        $query->fetch();
        if($id != 0) {
            $error .= '<li>' . t("user.error.email-exists") . '</li>';
        }
        $query->close();    
    }
    if(!isset($_POST['password']) || empty($_POST['password'])) {
        $error .= '<li>' . t("user.error.password") . '</li>';
    } elseif(!isset($_POST['repeat-password']) || $_POST['password'] != $_POST['repeat-password']) {
        $error .= '<li>' . t("user.error.repeat-password") . '</li>';
    }
    
    if(empty($error)) {
        $db->begin_transaction();
        $query = $db->prepare("INSERT INTO `" . DB_PRE . "users` (`password`, `member_since`) VALUES (?, ?);");
        $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $date = date("Y-m-d");
        $query->bind_param('ss', $hash, $date);
        $query->execute();
        $query->close();
        
        $key = barc_verify_email($db->insert_id, $_POST['email']);
        $db->commit();
        mail($_POST['email'], t("register.email.subject"), sprintf(t("register.email.content"), URL . "/verify-email/" . $key), "From: " . $barc_email_from);
        
    }
}

$content = "<h2>".t("nav.register")."</h2>";

if(!isset($_POST['submit']) || !empty($error)) {
    if(!empty($error)) {
        $content .= '<ul class="error">' . $error . '</ul>';   
    }
    $content .= user_form(true);    
} else {
    $content .= '<p class="success">' . t("register.success") . '</p>';
    if($barc_dev_mode) {
        $content .= '<p><a href="'. URL . "/verify-email/" . $key . '">[DEV VERIFY]</a></p>';    
    }
}

print_template($content);

?>