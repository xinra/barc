<?php

barc_load_translation("user");

$content = '<h2>'.t("reset-password.title").'</h2>';

if(isset($barc_args[0]) && !empty($barc_args[0])) {
    $key = $barc_args[0];
    $db = barc_open_db();
    $query = $db->prepare("SELECT `user_id` FROM `".DB_PRE."password_resets` WHERE `secret` = ? LIMIT 1;");
    $query->bind_param("s", $key);
    $query->execute();
    $query->bind_result($user_id);
    $query->fetch();
    $query->close();
    if($user_id == 0) {
        $content .= '<p class="error">'.t("reset-password.save.error").'</p>';
        print_template($content);
        die;
    } elseif(isset($_POST['submit'])) {
        if(!isset($_POST['password']) || empty($_POST['password'])) {
            $error = t("user.error.password");
        } elseif(!isset($_POST['repeat-password']) || $_POST['password'] != $_POST['repeat-password']) {
            $error = t("user.error.repeat-password");
        } else {
            $db->begin_transaction();
            $query = $db->prepare("DELETE FROM `".DB_PRE."password_resets` WHERE `user_id` = ?");
            $query->bind_param("i", $user_id);
            $query->execute();
            $query->close();

            $query = $db->prepare("UPDATE `".DB_PRE."users` SET `password` = ? WHERE `id` = ?;");
            $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $query->bind_param("si", $hash, $user_id);
            $query->execute();
            $query->close();
            $db->commit();
        }    
    }
    
    if(!isset($_POST['submit']) || isset($error)) {
        if(isset($error)) {
            $content .= '<p class="error">'.$error.'</p>';
        }
        $content .= '<form action="' . URL . '/reset-password/' . $key . '" method="post">';
            $content .= '<p><label for="password">' . t('user.password') . '</label> <input type="password" id="password" name="password" /></p>';
            $content .= '<p><label for="repeat-password">' . t('user.repeat-password') . '</label> <input type="password" id="repeat-password" name="repeat-password" /></p>';
            $content .= '<p><input type="submit" value="' . t('reset-password.save.button') . '" name="submit" /></p>';
        $content .= '</form>';    
    } else {
        $content .= '<p class="success">'.t("reset-password.save.success").'</p>';    
    }
    
} else {
    if(isset($_POST['submit'])) {
        if(!isset($_POST['email']) || empty($_POST['email'])) {
            $error = t("user.error.email");
        } else {
            $db = barc_open_db();
            $query = $db->prepare("SELECT `id` FROM `".DB_PRE."users` WHERE UPPER(`email`) = UPPER(?) LIMIT 1;");
            $query->bind_param("s", $_POST['email']);
            $query->execute();
            $query->bind_result($user_id);
            $query->fetch();
            $query->close();
            if($user_id == 0) {
                $error = t("reset-password.reset.error");
            } else {
                $query = $db->prepare("REPLACE INTO `" . DB_PRE . "password_resets` (`user_id`, `secret`, `expires`) VALUES (?, ?, ?);");
                $expires = date("Y-m-d", time()+$barc_password_reset_expires);
                $key = md5(password_hash(time(), PASSWORD_DEFAULT)); //this way the salt will be randomly generated
                $query->bind_param('iss', $user_id, $key, $expires);
                $query->execute();
                $query->close();
                mail($_POST['email'], t("reset-password.email.subject"), sprintf(t("reset-password.email.content"), URL . "/reset-password/" . $key), "From: " . $barc_email_from);
            }
        }
    }
    
    if(!isset($_POST['submit']) || isset($error)) {
        if(isset($error)) {
            $content .= '<p class="error">' . $error . '</p>';
        }
        $content .= '<form action="' . URL . '/reset-password" method="post">';
            $content .= '<p><label for="email">' . t('user.email') . '</label> <input type="email" id="email" name="email"' . (isset($_POST['email']) ? ' value="' . htmlspecialchars($_POST['email']) . '"' : '') . ' /></p>';
            $content .= '<p><input type="submit" value="' . t('reset-password.reset.button') . '" name="submit" /></p>';
            //For production you may want to include a captcha here
        $content .= '</form>';    
    } else {
        $content .= '<p class="success">' . t("reset-password.reset.success") . '</p>';
        if($barc_dev_mode) {
            $content .= '<p><a href="'. URL . "/reset-password/" . $key . '">[DEV VERIFY]</a></p>';
        }
    }   
}

print_template($content);
?>