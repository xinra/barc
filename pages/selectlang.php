<?php

$content = "<h2>Select Language</h2> <ul>"; //The select laguage page can not be internationalized!

$content .= '<li><a href="'.BASE_URL.'/en">English</a></li>';

/* You can iterate over all available languages which won't, however, provide language names - just locale names (shortcuts)
 
foreach($barc_available_langs as $lang) {
    $content .= '<li><a href="'.BASE_URL.'/'.$lang.'">'.$lang.'</a></li>';
}
*/

$content .= "</ul>";

print_template($content);

?>