<?php

if(isset($barc_args[0]) && !empty($barc_args[0])) {
    $key = $barc_args[0];
    $db = barc_open_db();
    $query = $db->prepare("SELECT `user_id`, `email` FROM `".DB_PRE."email_verifications` WHERE `secret` = ? LIMIT 1;");
    $query->bind_param("s", $key);
    $query->execute();
    $query->bind_result($id, $email);
    $query->fetch();
    $query->close();
    if($id == 0) {
        $error = t("verify-email.error");
    } else {
        $db->begin_transaction();
        $query = $db->prepare("DELETE FROM `".DB_PRE."email_verifications` WHERE `user_id` = ?");
        $query->bind_param("i", $id);
        $query->execute();
        $query->close();
        
        $query = $db->prepare("UPDATE `".DB_PRE."users` SET `email` = ? WHERE `id` = ?;");
        $query->bind_param("si", $email, $id);
        $query->execute();
        $query->close();
        $db->commit();
    }
} else {
    $error = t("verify-email.error");
}

if(isset($error)) {
    print_template('<p class="error">'.$error.'</p>');
} else {
    print_template('<p class="success">'.t("verify-email.success").'</p>');
}

?>